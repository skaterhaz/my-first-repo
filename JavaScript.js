﻿function colorme() {
    var table = document.getElementById("FullTable");
    var rows = table.getElementsByTagName("tr");
    var nameArray = [[0, 0]];
    var nameColor = true;
    var r = 1; var g = 1; var b = 1;

    for (var i = 1; i < rows.length; i++) {
        var cols = rows[i].getElementsByTagName("td");
        var name = cols[0].innerHTML;
        while (nameColor = true) {
            r = Math.floor(Math.random() * 255);
            g = Math.floor(Math.random() * 255);
            b = Math.floor(Math.random() * 255);
            var RGB = "rgb(" + r + ", " + g + ", " + b + ")";
            for (var nA = 0; nA < nameArray.length; nA++) {
                if (nameArray[nA][0] == name) {
                    rows[i].style.backgroundColor = nameArray[nA][1];
                    var done = true;
                    break;
                }
                else if (nameArray[nA][1] == RGB) {
                    break;
                }
                else if (nA == nameArray.length - 1) {
                    nameArray.push([cols[0].innerHTML, RGB]);
                    rows[i].style.backgroundColor = RGB;
                    var done = true;
                    break;
                }
            }
            if (done) { break; }
        }
    }
}

window.onload = colorme();

function ShowError(textBox){
    document.getElementById(message).style.display = "inline";
}

$('.linkbuttons').click(function () {
    trid = $(this).closest('tr').attr('id');
    td1 = $(this).closest('tr').children('td:nth-child(1)').text();
    td2 = $(this).closest('tr').children('td:nth-child(2)').text();
    td3 = $(this).closest('tr').children('td:nth-child(3)').text();
    var arr = td1.split(" ");
    $('#FirstNameBox').val(arr[0]);
    $('#LastNameBox').val(arr[1]);
    $('#TelephoneBox').val(td2);
    $('#DOBBox').val(td3);
    
    $('#ButtonX').val("Edit");
    $('#ButtonX').css('background-color', '#ff8080');
    $('#hiddenRef').val(trid);
    $('#hiddenText').val("Edit");
    $('#cancelButt').text("cancel");

})

$('#cancelButt').click(function () {
    $('#FirstNameBox').val("");
    $('#LastNameBox').val("");
    $('#TelephoneBox').val("");
    $('#DOBBox').val("");
    $('#hiddenRef').val(null);
    $('#hiddenText').val("Add");
    $('#ButtonX').val("Add");
    $('#ButtonX').css('background-color', '#99ff99');
    $('#cancelButt').text("");
});
