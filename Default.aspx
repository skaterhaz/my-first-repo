﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer database</title>
    <link = type="text/css" rel="stylesheet" href="StyleSheet.css" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
</head>
<body>
    <form id="form1" runat="server" method="POST">
        <h1>Customer Entry Form</h1>
        <hr />
        <br />
        <div class="container">
            <div class="InputAreas">
                <label class="InputLabel">First Name</label><br />
                <input id="FirstNameBox" class="UserInputs" type="text" runat="server" /><br />
                <label id="FNError" class="ErrorLabel" runat="server">Enter a first name</label><br />
            </div>

            <div class="InputAreas">
                <label class="InputLabel">Surname</label><br />
                <input id="LastNameBox" class="UserInputs" type="text" runat="server" /><br />
                <label id="SError" class="ErrorLabel" runat="server">Enter a surname</label><br />
            </div>
            
            <div class="InputAreas">
                <label class="InputLabel">Telephone</label><br />
                <input id="TelephoneBox" class="UserInputs" type="text" runat="server" /><br />
                <label id="TeleError" class="ErrorLabel" runat="server">Enter a phone number</label><br />
            </div>
            
            <div class="InputAreas">
                <label class="InputLabel">Date of birth</label><br />
                <input id="DOBBox" class="UserInputs" type="text" runat="server" /><br />
                <label id="DOBError" class="ErrorLabel" runat="server">"dd/mm/yyyy"</label><br />
            </div>
            
            <div class="InputAreas">
                <input id="hiddenRef" type="hidden" runat="server"/>
                <input id="hiddenText" type="hidden" runat="server"/>
                <input id="ButtonX" runat="server" onserverclick="ButtonClick" value="Add" type="submit"/><br />
                <label id="cancel" runat="server"><a id="cancelButt" href="#"></a></label>
            </div>
            
        </div> 

        <hr />
        <br />

        <table id ="FullTable" border = '1' >
            <tr>
                <th>Customer Name</th>
                <th>Telephone</th>
                <th>Date of birth</th>
                <th>Age</th>
                <th>Edit</th>
            </tr>
            <% foreach(Customer cust in custArray){ %>
                <tr id="<%=cust.DBid %>">
                    <td><%=cust.CustomerName%></td>
                    <td><%=cust.Telephone%></td>
                    <td><%=cust.DOB%></td>
                    <td><%=cust.Age%></td>
                    <td><a class="linkbuttons" href="#">Edit</a></td>
                </tr>
            <%} %>

        </table>
        
      <!--  <asp:PlaceHolder ID = "PlaceHolder1" runat="server" /> -->

    </form>

    <script type="text/javascript" src="JavaScript.js" ></script>
</body>
</html>