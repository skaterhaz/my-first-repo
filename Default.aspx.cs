﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;


public partial class _Default : System.Web.UI.Page
{
    public List<Customer> custArray { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        string connString = ConfigurationManager.ConnectionStrings["myConnString"].ConnectionString;

        using (SqlConnection conn = new SqlConnection(connString))
        {
            using (SqlCommand cmd = new SqlCommand("getcustomers", conn))
            {

                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    custArray = new List<Customer>();
                    while (reader.Read())
                    {
                        var customer = new Customer();
                        customer.CustomerName = reader[1].ToString() + " " + reader[2].ToString();
                        customer.Telephone = reader[3].ToString();

                        string DOB = reader[4].ToString();
                        DateTime today = DateTime.Today;
                        DateTime DOBx = DateTime.Parse(DOB);
                        int age = today.Year - DOBx.Year;
                        if (DOBx > today.AddYears(-age)) { age--; }

                        customer.DOB = DOBx.ToString("dd/MM/yyyy");
                        customer.Age = age;
                        customer.DBid = "ref" + reader[0].ToString();

                        custArray.Add(customer);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
        }
    }

    private DataTable GetData()
    {
        string connString = ConfigurationManager.ConnectionStrings["myConnString"].ConnectionString;

        using (SqlConnection conn = new SqlConnection(connString))
        {
            using (SqlCommand cmd = new SqlCommand("getcustomers", conn))
            {
                SqlDataAdapter da = new SqlDataAdapter();
                DataTable dt = new DataTable();

                cmd.Connection = conn;
                da.SelectCommand = cmd;

                da.Fill(dt);
                return dt;
            }
        }
    }
    protected void ButtonClick(object sender, EventArgs e)
    {
        var returnback = false;
        if (String.IsNullOrEmpty(FirstNameBox.Value))
        { 
            FNError.Attributes.Add("Style", "Display:inline"); returnback = true; 
        }
        else 
        { 
            FNError.Attributes.Add("Style", "Display:none"); 
        }

        if (String.IsNullOrEmpty(LastNameBox.Value))
        { 
            SError.Attributes.Add("Style", "Display:inline"); returnback = true; 
        }
        else 
        { 
            SError.Attributes.Add("Style", "Display:none"); 
        }

        if (String.IsNullOrEmpty(TelephoneBox.Value))
        { 
            TeleError.Attributes.Add("Style", "Display:inline"); returnback = true; 
        }
        else 
        { 
            TeleError.Attributes.Add("Style", "Display:none"); 
        }

        DateTime dob;
        if (DateTime.TryParse(DOBBox.Value, out dob))
        {
            DOBError.Attributes.Add("Style", "Display:none"); 
            
        }
        else 
        {
            DOBError.Attributes.Add("Style", "Display:inline"); returnback = true; 
        }

        if (returnback) { return; }


        string connString = ConfigurationManager.ConnectionStrings["myConnString"].ConnectionString;

        using (SqlConnection conn = new SqlConnection(connString))
        {

            bool editButt = false;
            string addCustomer = "INSERT into Customer (FirstName, Surname, Telephone, DOB) VALUES (@FN, @SN, @TP, @DOB)";

            if (hiddenText.Value == "Edit")
            {
                addCustomer = "UPDATE Customer SET FirstName=@FN, Surname=@SN, Telephone=@TP, DOB=@DOB WHERE Cust_ID = @ref";
                editButt = true;
            }

            using (SqlCommand cmd = new SqlCommand(addCustomer, conn))
            {
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@FN", FirstNameBox.Value);
                cmd.Parameters.AddWithValue("@SN", LastNameBox.Value);
                cmd.Parameters.AddWithValue("@TP", TelephoneBox.Value);
                cmd.Parameters.AddWithValue("@DOB", Convert.ToDateTime(DOBBox.Value));
                if (editButt)
                {
                    var refID = hiddenRef.Value;
                    cmd.Parameters.AddWithValue("@ref", refID.Substring(3, refID.Length - 3));
                }

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        FirstNameBox.Value = "";
        LastNameBox.Value = "";
        TelephoneBox.Value = "";
        DOBBox.Value = "";

        Response.Redirect("~/default.aspx");
    }

}